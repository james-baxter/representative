# GoodCitizen
iOS App:  Info on user's govt. reps and upcoming elections


==================================
Final Project App -- Udacity iOS Developer Nanodegree June 2016
==================================

Build
-----
- Requires SwiftyJSON and KingFisher Pods.  Please open workspace to build.

Description
-----------
- Downloads information on upcoming elections and user's government representatives from Google Civic Information API.
- https://developers.google.com/civic-information/
- Representative data is on an address-by-address basis, offering information on local politicians.
- User can click on a representative to see photo, office, party, and contact info.
- Clicking on phone initiates phone call, twitter launches twitter app or goes to the appropriate webpage, etc.
- If there is an upcoming election for that address, the user can see the date, location, and candidates running in the various contests.  A map is shown which will launch Maps.
- After first launch when user is prompted to enter address, data is persisted.
- User can use current GPS location in the address form.

Use
---
- Enter voter's address when prompted or by using the Search button.
- Click on desired level of government, then click on a representative.
- Click on social media, phone, email, etc.
- Click on Upcoming Elections if available.  Click on map or contest.
- Data is very different on an address-by-address basis, esp. for elections that have varying degrees of completeness.
- On the address entry page I have included an initial set of addresses to be tested (click them and hit submit).  Any address that is entered by the user is added to the test list and persisted for later testing.
- Very few states have elections at any given time.  As of June 8th there are elections in WA and MN.  I have put these test addresses at the top of the list (after an invalid address to test failure).
- For addresses with no upcoming elections, Google offers the option of a test election ID=2000.  Click the "Test Elections" switch on the address form to show an earlier election for that address.
- The API URL is printed to the console for viewing JSON in web browser.

//
//  AddressStruct.swift
//  Representative
//
//  Created by Dolemite on 4/16/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct AddressStruct {
    var street: String
    var city: String
    var state: String
    var zip: String

    var unabbreviateState: String {
        return states[state]!
    }

    var cityStateZip: String {
        return "\(city), \(state)  \(zip)"
    }

    func addressString(withPercentCoding percentCoded: Bool) -> String {
        var array = [String]()

        if street != "" {
            array.append(street)
        }
        if city != "" {
            array.append(city)
        }
        if state != "" {
            if array.isEmpty { // if no address available, don't abbreviate state
                array.append(states[state.lowercased()] ?? state)
            } else {
                array.append(state)
            }
        }
        if zip != "" {
            array.append(zip)
        }

        if percentCoded {
            let string = array.joined(separator: " ")
            return string.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        } else {
            if zip != "" {  // zip should not be separated from state by a comma
                return array.dropLast().joined(separator: ", ") + " " + array.last!
            } else {
                return array.joined(separator: ", ")
            }
        }
    }

    init(street: String, city: String, state: String, zip: String) {
        self.street = street
        self.city = city
        self.state = state.uppercased()
        self.zip = zip
    }

    init(json: JSON) {
        street = json["line1"].stringValue
        city = json["city"].stringValue
        state = json["state"].stringValue.lowercased()
        zip = json["zip"].stringValue
    }
}

let states = [
    "al": "Alabama",
    "ak": "Alaska",
    "az": "Arizona",
    "ar": "Arkansas",
    "ca": "California",
    "co": "Colorado",
    "ct": "Connecticut",
    "de": "Delaware",
    "fl": "Florida",
    "ga": "Georgia",
    "hi": "Hawaii",
    "id": "Idaho",
    "il": "Illinois",
    "in": "Indiana",
    "ia": "Iowa",
    "ks": "Kansas",
    "ky": "Kentucky",
    "la": "Louisiana",
    "me": "Maine",
    "md": "Maryland",
    "ma": "Massachusetts",
    "mi": "Michigan",
    "mn": "Minnesota",
    "ms": "Mississippi",
    "mo": "Missouri",
    "mt": "Montana",
    "ne": "Nebraska",
    "nv": "Nevada",
    "nh": "New Hampshire",
    "nj": "New Jersey",
    "nm": "New Mexico",
    "ny": "New York",
    "nc": "North Carolina",
    "nd": "North Dakota",
    "oh": "Ohio",
    "ok": "Oklahoma",
    "or": "Oregon",
    "pa": "Pennsylvania",
    "ri": "Rhode Island",
    "sc": "South Carolina",
    "sd": "South Dakota",
    "tn": "Tennessee",
    "tx": "Texas",
    "ut": "Utah",
    "vt": "Vermont",
    "va": "Virginia",
    "wa": "Washington",
    "wv": "West Virginia",
    "wi": "Wisconsin",
    "wy": "Wyoming",
]

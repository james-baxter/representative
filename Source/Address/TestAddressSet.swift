//
//  TestAddressSet.swift
//  Representative
//
//  Created by Dolemite on 5/21/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import Foundation
import CoreData

/* 
 * Addresses available in New Address form for testing the app.  Each new address
 * entered by user is added to the set and available later.
 */

class TestAddressSet: NSManagedObject {

    @NSManaged var addresses: [ManagedAddress]

    override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    init(context: NSManagedObjectContext) {
        let entity = NSEntityDescription.entity(forEntityName: "TestAddressSet", in: context)
        super.init(entity: entity!, insertInto: context)

        initWithAddresses(context)
    }

    func initWithAddresses(_ context: NSManagedObjectContext) {

        let initialAddresses: [AddressStruct] = [
            AddressStruct(street: "142 Transit St", city: "Providence", state: "RI", zip: "02906"),
            AddressStruct(street: "Failure Example Street", city: "Kittenville", state: "Fail", zip: "666"),
            AddressStruct(street: "115 Alameda Dr", city: "Kelso", state: "WA", zip: "98626"),
            AddressStruct(street: "20630 Happy Hollow Rd", city: "Grand Rapids", state: "MN", zip: "55744"),
            AddressStruct(street: "168 Pleasant Hill Rd #A", city: "Harrisonburg", state: "VA", zip: "22801"),
            AddressStruct(street: "3519 Stonewall St", city: "Bismarck", state: "ND", zip: "58503"),
            AddressStruct(street: "94 Alder Lane", city: "Hardeeville", state: "SC", zip: "29927"),
            AddressStruct(street: "318 Farragut St NW", city: "Washington", state: "DC", zip: ""),
            AddressStruct(street: "3524 McIntosh Lane", city: "Snellville", state: "GA", zip: "30039"),
            AddressStruct(street: "312 Philadelphia Ave", city: "Takoma Park", state: "MD", zip: ""),
            AddressStruct(street: "1208 W High St", city: "Haddon Heights", state: "NJ", zip: ""),
            AddressStruct(street: "134-74 231 St", city: "Queens", state: "NY", zip: ""),
            AddressStruct(street: "1153 W 19th St", city: "San Pedro", state: "CA", zip: ""),
            AddressStruct(street: "14965 Himebaugh Ave", city: "Omaha", state: "NE", zip: ""),
            AddressStruct(street: "675 E 5140 N", city: "Unoch", state: "UT", zip: ""),
            AddressStruct(street: "902 N Maple", city: "Watertown", state: "SD", zip: ""),
            AddressStruct(street: "5760 NW Cleburn Dr", city: "Port St Lucie", state: "FL", zip: ""),
            AddressStruct(street: "333 NE 21st Ave, APT. 300", city: "Deerfield Beach", state: "FL", zip: "")
        ]

        let _ = initialAddresses.map() {
            let addr = ManagedAddress(address: $0, context: context)
            addr.testSet = self
        }
    }

}

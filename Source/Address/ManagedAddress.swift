//
//  ManagedAddress.swift
//  Representative
//
//  Created by Dolemite on 4/16/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import Foundation
import CoreData

class ManagedAddress: NSManagedObject {

   @NSManaged var street: String
   @NSManaged var city: String
   @NSManaged var state: String
   @NSManaged var zip: String

   @NSManaged var constituent: Constituent?
   @NSManaged var testSet: TestAddressSet?

   var addressStruct: AddressStruct {
      return AddressStruct(street: street, city: city, state: state, zip: zip)
   }

   override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
      super.init(entity: entity, insertInto: context)
   }

   init(address: AddressStruct, context: NSManagedObjectContext) {
      let entity = NSEntityDescription.entity(forEntityName: "Address", in: context)
      super.init(entity: entity!, insertInto: context)

      street = address.street
      city = address.city
      state = address.state
      zip = address.zip
   }
}

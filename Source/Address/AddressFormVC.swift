//
//  AddressFormVC.swift
//  Representative
//
//  Created by Dolemite on 5/1/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import CoreLocation

extension CLAuthorizationStatus: CustomStringConvertible {
    public var description: String {
        switch self {
        case .notDetermined:
            return "Not Determined"
        case .restricted:
            return "restricted"
        case .denied:
            return "denied"
        case .authorizedAlways:
            return "always"
        case .authorizedWhenInUse:
            return "When in use"
        @unknown default:
            return "Unknown CLAuthorizationStatus case"
        }
    }
}

// New voter address to look up
class AddressFormVC: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {

    // MARK: - Outlets

    @IBOutlet weak var zip: UITextField!
    @IBOutlet weak var state: UITextField!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var street: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var locationButton: UIButton!

    // Test outlets
    @IBOutlet weak var testElectionSwitch: UISwitch!
    @IBOutlet weak var testElectionsLabel: UILabel!
    @IBOutlet weak var testAddressesLabel: UILabel!
    @IBOutlet weak var testAddressesLabelStack: UIStackView!
    @IBOutlet weak var testAddressesTableContainer: UIView!

    // MARK: - Variables

    let cd = CoreDataStackManager.shared
    let mainContext = CoreDataStackManager.shared.mainContext
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let spinner = UIActivityIndicatorView(style: .whiteLarge)

    var testTable: UITableView!
    var testAddressSet: TestAddressSet!
    var testAddresses: [AddressStruct]!
    var newAddress = false // if true, current address is added to test address set

    var locationManager: CLLocationManager!
    var userLocation: AddressStruct? = nil
    var locationStatus: CLAuthorizationStatus?
    var apiRequestTask: URLSessionDataTask?

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // UITextField delegates
        street.delegate = self
        state.delegate = self
        city.delegate = self
        zip.delegate = self

        // Activity indicator
        spinner.hidesWhenStopped = true
        spinner.center = view.center
        spinner.color = UIColor.purple
        view.addSubview(spinner)

        // Test Addresses
        #if RELEASE
        testAddressesLabelStack.removeFromSuperview()
        testAddressesTableContainer.removeFromSuperview()
        clearButton.removeFromSuperview()
        #else
        loadTestAddresses() // for picker
        testElectionSwitch.addTarget(self, action: #selector(AddressFormVC.toggleUseOfTestElection), for: UIControl.Event.valueChanged)
        testElectionSwitch.transform = CGAffineTransform(scaleX: 0.60, y: 0.60);

        testTable = (children[0] as! UITableViewController).tableView
        testTable.delegate = self
        testTable.dataSource = self
        #endif

        setupLocationManager()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // Populate fields with saved constituent data
        if let addr = appDelegate.constituent?.address?.addressStruct {
            populateFields(addr)
        }

        cancelButton.layer.cornerRadius = 10
        submitButton.layer.cornerRadius = 10
        clearButton.layer.cornerRadius = 10

        view.backgroundColor = UIColor.groupTableViewBackground

        locationButton.alpha = 1.0
        locationButton.isEnabled = true

        #if DEBUG
        if let useTestElection = appDelegate.constituent?.useTestElection {
            testElectionSwitch.isEnabled = true
            testElectionSwitch.setOn(useTestElection, animated: true)  // or off: useTestElection is bool
        } else {
            testElectionSwitch.isEnabled = false
        }
        #endif
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // avoid crash when constituent's address is nil
        if let constituent = appDelegate.constituent, constituent.address == nil {
            appDelegate.constituent = nil
        }
    }

    // MARK: - Current user location

    func setupLocationManager() {

        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.distanceFilter = 6.0
        locationManager.delegate = self
    }

    // "use gps" button
    @IBAction func locateUser(_ sender: UIButton) {

        let locAuthStatus = CLLocationManager.authorizationStatus()

        switch locAuthStatus {

        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.requestLocation()
            startSpinner()

        case .notDetermined:
            locationManager.requestWhenInUseAuthorization() // system dialog

        case .restricted, .denied:
            var message: String
            var settingsURL: URL? = nil

            if locAuthStatus == .denied {  // App settings or device settings
                if CLLocationManager.locationServicesEnabled() {
                    message = "Good Citizen needs permission to use GPS.  You can change this in Settings."
                    settingsURL = URL(string: UIApplication.openSettingsURLString)
                } else {
                    message = "Location services are turned off on this device.  You can change these in Settings."
                }
            } else { // .restricted
                message = "Location services are restricted.  You can change these in Settings: Restrictions."
            }

            let alertVC = UIAlertController(title: "GPS Unavailable", message: message, preferredStyle: .alert)
            alertVC.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))

            // If denied because of GoodCitizen's settings, add option to open its settings
            if let url = settingsURL {
                let settingsAction = UIAlertAction(title: "Change in Settings", style: .default) { _ in
                    DispatchQueue.main.async {
                        UIApplication.shared.open(url)
                    }
                }
                alertVC.addAction(settingsAction)
            }

            self.present(alertVC, animated: true, completion: nil)

        @unknown default:
            fatalError("Unknown CLLocationManager.authorizationStatus")
        }
    }

    // CLLocationManagerDelegate:

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        let geocoder = CLGeocoder()

        if let location = locations.last {

            geocoder.reverseGeocodeLocation(location) { placemarks, error in
                DispatchQueue.main.async {
                    self.stopSpinner()
                }
                guard error == nil else {

                    self.presentSimpleAlert("Failed to look up the address for this location.\nCheck your network connection.",
                                            handler: nil)
                    self.userLocation = nil

                    return
                }

                let firstPlacemark = placemarks?.first
                let street = firstPlacemark?.name ?? ""
                let city = firstPlacemark?.locality ?? ""
                let state = firstPlacemark?.administrativeArea ?? ""
                let zip = firstPlacemark?.postalCode ?? ""

                self.userLocation = AddressStruct(street: street, city: city, state: state, zip: zip)
                self.populateFields(self.userLocation!)
            }
        }
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {

        switch status {

        case .authorizedAlways, .authorizedWhenInUse:
            if let locStatus = locationStatus, locStatus == .notDetermined {
                startSpinner()
                locationManager.requestLocation() // User requested GPS turned on in system dialog
            }
            locationButton.alpha = 1.0

        case .notDetermined:
            locationButton.alpha = 1.0

        default:
            locationButton.alpha = 0.35
        }

        locationStatus = status
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        NSLog("locationManager:didFailWithError: \(error)")
        stopSpinner()
    }

    // MARK: - Heavy Lifting

    func populateFields(_ address: AddressStruct) {
        street.text = address.street.capitalized
        city.text = address.city.capitalized
        state.text = address.state.uppercased()
        zip.text = address.zip
    }

    func loadTestAddresses() {

        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TestAddressSet")

        do {
            testAddressSet = (try mainContext.fetch(fetchRequest) as! [TestAddressSet]).first
        } catch let error as NSError {
            presentSimpleAlert(error.localizedDescription, handler: nil)
            return
        }

        // Check if there is a test set on disk, otherwise create a new set
        if let set = testAddressSet {
            testAddresses = set.addresses.map() {
                return $0.addressStruct
            }
        } else {
            let set = TestAddressSet(context: mainContext)
            testAddresses = set.addresses.map() {
                return $0.addressStruct
            }
            cd.saveContext(mainContext)
        }
    }

    // testElectionSwitch target action
    // CIA client can add "id=2000" to its election query, which generates test data
    @objc func toggleUseOfTestElection() {
        let useTestElection = appDelegate.constituent!.useTestElection
        appDelegate.constituent!.useTestElection = !useTestElection
    }

    @IBAction func cancel(_ sender: UIButton) {
        if let task = apiRequestTask {
            task.cancel()
        }
        locationManager.stopUpdatingLocation()
        stopSpinner()
    }

    @IBAction func submit(_ sender: UIButton) {
        view.endEditing(false)
        startSpinner()

        let address = AddressStruct(street: street.text!, city: city.text!, state: state.text!, zip: zip.text!)

        // Add new address to test addresses
        if newAddress {
            let addrToBeSaved = ManagedAddress(address: address, context: mainContext)
            addrToBeSaved.testSet = testAddressSet
            cd.saveContext(mainContext)
        }

        if appDelegate.constituent == nil {
            appDelegate.constituent = Constituent(context: mainContext)
        }

        // Representative Info
        apiRequestTask = appDelegate.constituent!.getRepInfo(address) { jsonData, errorString in

            guard errorString == nil else {
                DispatchQueue.main.async() {
                    self.stopSpinner()
                    if errorString != "cancelled" {
                        self.presentSimpleAlert(errorString!, handler: nil)
                    }
                }
                return
            }

            // Election Info
            self.appDelegate.constituent!.getElectionInfo(address) { errorString in
                DispatchQueue.main.async() { self.stopSpinner() }

                guard errorString == nil else {
                    self.presentSimpleAlert(errorString!, handler: nil)
                    return
                }

                self.cd.saveContext(self.mainContext)
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }

    @IBAction func clear(_ sender: UIButton) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Constituent")
        fetchRequest.sortDescriptors = []

        do {
            if let constituent = try mainContext.fetch(fetchRequest).first as? Constituent {
                mainContext.delete(constituent)
                cd.saveContext(mainContext)
                appDelegate.constituent = nil
            }
        } catch {
            assertionFailure(error.localizedDescription)
        }

        self.navigationController?.popViewController(animated: true)
    }


    func presentSimpleAlert(_ string: String, handler: ((_ alert: UIAlertAction) -> Void)?) {
        let alert = UIAlertController(title: "Alert", message: string, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: handler))
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }

    fileprivate func startSpinner() {
        view.alpha = 0.5
        spinner.startAnimating()
    }

    fileprivate func stopSpinner() {
        view.alpha = 1.0
        spinner.stopAnimating()
    }


    // MARK: - Text Field Delegate

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        switch textField {

        case street:
            city.becomeFirstResponder()
        case city:
            state.becomeFirstResponder()
        case state:
            // Check for OH, CA, ND, etc.
            if state.text!.count > 0 && states[state.text!.lowercased()] == nil {
                return false
            } else {
                zip.becomeFirstResponder()
            }
        case zip:
            let length = zip.text!.count
            if length > 0 && length < 5 { // Allowed to be blank, otherwise 5 digits
                return false
            } else {
                textField.resignFirstResponder()
            }
        default:
            break
        }

        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        newAddress = true // notifies us that we will save this as a new test address

        if range.length > 0 {
            return true
        }

        switch textField {

        case state:
            let charset = CharacterSet.letters.inverted
            if let _ = string.rangeOfCharacter(from: charset) {
                return false
            }
            if textField.text!.count >= 2 {
                return false
            }

        case zip:
            let charset = CharacterSet.decimalDigits.inverted
            if let _ = string.rangeOfCharacter(from: charset) {
                return false
            }
            if textField.text!.count >= 5 {
                return false
            }
        default:
            return true
        }

        return true
    }

    // MARK: - Test addresses table data source

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return testAddresses.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Test Address", for: indexPath)
        cell.textLabel?.text = testAddresses[indexPath.row].street + " (\(testAddresses[indexPath.row].state))"
        let colorView = UIView()
        colorView.backgroundColor = UIColor.lightGray
        cell.selectedBackgroundView = colorView
        return cell
    }

    // MARK: - Test addresses table delegate

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        populateFields(testAddresses[indexPath.row])
        newAddress = false // we will not resave this in the testAddressSet
    }
}

//
//  CoreDataStackManager.swift
//

import Foundation
import CoreData

private let SQLITE_FILE_NAME = "goodcitizen.sqlite"

/*
*  Adapted from Udacity example
*/
class CoreDataStackManager {

   static let shared = CoreDataStackManager()

   lazy var applicationDocumentsDirectory: URL = {
      let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
      return urls[urls.count-1]
   }()

   lazy var managedObjectModel: NSManagedObjectModel = {
      let modelURL = Bundle.main.url(forResource: "GoodCitizen", withExtension: "momd")!
      return NSManagedObjectModel(contentsOf: modelURL)!
   }()

   lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {

      let coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
      let url = self.applicationDocumentsDirectory.appendingPathComponent(SQLITE_FILE_NAME)

      var failureReason = "There was an error creating or loading the application's saved data."

      do {
         try coordinator!.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
      } catch {
         var dict = [String: AnyObject]()
         dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject
         dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject
         let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
         NSLog(wrappedError.localizedDescription)
         abort()
      }

      return coordinator
   }()

   // Main Context
   lazy var mainContext: NSManagedObjectContext = {
      let coordinator = self.persistentStoreCoordinator
      var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
      managedObjectContext.persistentStoreCoordinator = coordinator

      return managedObjectContext
   }()

   // Private Child Context
   func createPrivateMOC() -> NSManagedObjectContext {
      let moc = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
      moc.parent = mainContext
      return moc
   }

   func clearCoreData() {

      var requests = [NSFetchRequest<NSFetchRequestResult>]()
      let entities = managedObjectModel.entitiesByName

      for entity in entities {
         requests.append(NSFetchRequest(entityName: entity.0))
      }

      let _ = requests.map() { request in
         let batchDelete = NSBatchDeleteRequest(fetchRequest: request)
         do {
            try persistentStoreCoordinator?.execute(batchDelete, with: mainContext)
         } catch let error as NSError {
            fatalError("\(error)")
         }
      }

      saveContext(mainContext)
   }

   // MARK: - Core Data Saving support

   // Recursive save for child contexts
   func saveContext(_ context: NSManagedObjectContext) {

      context.performAndWait {
         if context.hasChanges {
            do {
               try context.save()
            } catch let error as NSError {
               NSLog("Unresolved error \(error), \(error.userInfo)")
               abort()
            }

            if let parent = context.parent {
               self.saveContext(parent)
            }
         }
      }
   }
}

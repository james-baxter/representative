//
//  CIA_Constants.swift
//  Representative
//

import Foundation

//  Data from Google Civic Information API
//  https://developers.google.com/civic-information/


struct CIA_Constants {

   static let baseURL = "https://www.googleapis.com/civicinfo/v2"
   static let apiKey = "AIzaSyDXwo3ZUqp29RoqvpOtpCgMkJiS_PHOeNc"

   enum Methods: String {
      case electionQuery = "elections"  // Upcoming elections (all)
      case voterInfoQuery = "voterinfo"     // For specific election & address
      case representativeInfoByAddress = "representatives"
      // There are additional methods for this API not used here
   }
}

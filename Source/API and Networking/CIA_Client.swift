//
//  CIA_Client.swift
//  Created by Dolemite on 4/10/16.
//

import Foundation
import UIKit
import CoreData
import SwiftyJSON

//  API client for the Google Civic Information API
//  https://developers.google.com/civic-information/

class CIA_Client {

    // MARK:  Properties

    static let shared = CIA_Client()
    let urlSession = URLSession.shared
    let timeout: Double
    let appDelegate: AppDelegate
    let constituent: Constituent?

    // MARK:  Main Functions

    init() {
        appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        constituent = appDelegate.constituent
        timeout = appDelegate.networkTimeout
    }

    // Download divisions, offices, and officials ("representatives" API method)
    func getRepresentativeInfo(_ address: AddressStruct, handler: @escaping (_ jsonData: Data?, _ errorString: String?) -> Void) -> URLSessionDataTask {

        let url = URL(string: generateURLString(CIA_Constants.Methods.representativeInfoByAddress.rawValue, address: address))!

        #if DEBUG
        print(url)
        #endif

        let request = URLRequest(url: url, timeoutInterval: timeout)

        let task = makeAPIRequest(request) { json, errorString in

            if let _ = errorString {
                handler(json, errorString!)
            } else {
                handler(json, nil)
            }
        }
        return task
    }

    func allElections(handler: @escaping (_ jsonData: Data?, _ errorString: String?) -> Void) {

        let url = URL(string: generateURLString(CIA_Constants.Methods.electionQuery.rawValue, address: nil))!
        let request = URLRequest(url: url, timeoutInterval: timeout)

        let _ = makeAPIRequest(request) { json, errorString in
            if let errorString = errorString {
                handler(json, errorString)
            } else {
                handler(json, nil)
            }
        }
    }

    func getElectionInfo(_ address: AddressStruct, electionID: Int? = nil, handler: @escaping (_ jsonData: Data?, _ errorString: String?) -> Void) {

        let url = URL(string: generateURLString(CIA_Constants.Methods.voterInfoQuery.rawValue, address: address, electionID: electionID))!

        #if DEBUG
        print(url)
        #endif

        let request = URLRequest(url: url, timeoutInterval: timeout)

        let _ = makeAPIRequest(request) { json, errorString in

            if let _ = errorString {
                handler(json, errorString!)
            } else {
                handler(json, nil)
            }
        }
    }

    func makeAPIRequest(_ request: URLRequest, handler: @escaping (_ json: Data?, _ error: String?) -> Void) -> URLSessionDataTask {

        let task = self.urlSession.dataTask(with: request, completionHandler: { data, response, error in

            // e.g., no Internet
            guard error == nil else {
                handler(nil, error!.localizedDescription)
                return
            }

            // e.g., bad residential address
            if let resp = response as? HTTPURLResponse, resp.statusCode != 200 && resp.statusCode != 400 {
                do {
                    let json = try JSON(data: data!)
                    handler(data, json["error"]["message"].stringValue)
                } catch {
                    handler(nil, "Failed to decode JSON")
                }
                return
            }

            // looks like we have a winner
            if let data = data {
                handler(data, nil)
            }
        })
        task.resume()
        return task
    }

    func generateURLString(_ method: String, address: AddressStruct?, electionID: Int? = nil) -> String {
        var addressString: String
        var params: [String: String]

        if let address = address {
            addressString = address.addressString(withPercentCoding: true)
            params = ["address": addressString, "key": CIA_Constants.apiKey]
        } else {
            params = ["key": CIA_Constants.apiKey]
        }

        if let id = electionID {
            params["electionId"] = String(id)
        }

        if let constituent = constituent, method == CIA_Constants.Methods.voterInfoQuery.rawValue && constituent.useTestElection {
            params["electionId"] = "2000"
        }

        let methodString = CIA_Constants.baseURL + "/" + method + "?"
        let paramStrings = params.map() { return $0 + "=" + $1 }
        return methodString + paramStrings.joined(separator: "&")
    }
}

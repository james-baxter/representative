//
//  NationalReps.swift
//  Representative
//
//  Created by Dolemite on 4/13/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import Foundation
import CoreData

// Encapsulates country-level reps, e.g., Pres & VP
// Owned by Constituent, used by National Table to organize president, etc.

struct NationalReps {

   var president: Official!
   var vp: Official!
   var senators: [Official]?
   var congressperson: Official!

   init(officials: [Official]) {

      for official in officials {

         let level = official.office.levels?.first ?? ""
         let role = official.office.roles?.first ?? ""

         switch (level, role) {

         case ("country", "headOfGovernment"), ("country", "headOfState"):
            president = official
         case ("country", "deputyHeadOfGovernment"):
            vp = official
         case ("country", "legislatorUpperBody"):
            if senators == nil {                    // DC has not senators
               senators = [Official]()
            }
            senators!.append(official)
         case ("country", "legislatorLowerBody"):
            congressperson = official
         default:
            break
         }
      }
   }
}

//
//  Division.swift
//
//  Created by Dolemite on 4/11/16.
//

import Foundation
import CoreData
import SwiftyJSON

// Division, e.g., National, state, town 
// https://developers.google.com/civic-information/docs/v2/divisions#resource-representations

enum DivisionLevel {
   case national, state, local
}

open class Division {

   var ocdID = String()
   var name = String()
   var offices: [Office]? // Divisions map to offices in JSON

   let appDelegate = UIApplication.shared.delegate as! AppDelegate

   // Constituent's state
   var state: String {
      var myState = String()
      CoreDataStackManager.shared.mainContext.performAndWait {
         myState = self.appDelegate.constituent!.state
      }
      return myState
   }


   init(ocdId: String, json: JSON) {

      ocdID = ocdId
      name = json["name"].stringValue
   }

   lazy var level: DivisionLevel = {

      let ocd = self.ocdID

      switch ocd {

      // Pres & VP
      case _ where ocd.hasSuffix("country:us"):
         return DivisionLevel.national

      // US Congress
      case _ where ocd.contains("state:\(self.state)/cd:"):
         return DivisionLevel.national

      case _ where ocd.hasSuffix("state:\(self.state)"):
         return DivisionLevel.state

      // State Legislature
      case let ID where ID.contains("sldu:") || ID.contains("sldl:"):
         return DivisionLevel.state

      // Washington DC
      case _ where ocd.contains("district:dc/ward:"):
         return DivisionLevel.local

      case _ where ocd.contains("district:dc"):
         return DivisionLevel.state

      default:
         // god willing, non-local stuff will have been caught above.
        //    Local will involve counties, places, wards, precincts, council districts, etc ...
         return DivisionLevel.local
      }
   }()
}

//
//  RepInfoVC.swift
//  Representative
//
//  Created by Dolemite on 4/27/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import UIKit
import SwiftyJSON

class RepInfoVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

   // MARK: Outlets

   @IBOutlet weak var repName: UILabel!
   @IBOutlet weak var officeName: UILabel!
   @IBOutlet weak var party: UILabel!
   @IBOutlet weak var photo: UIImageView!
   var contactTable: UITableView!

   // MARK: Variables

   var official: Official!
   var contactInfo: [Contact]?

   // MARK: Lifecycle
   override func viewDidLoad() {
      super.viewDidLoad()

      // Mail, phone, facebook, etc.
      contactTable = (children[0] as! UITableViewController).tableView
      contactTable.dataSource = self
      contactTable.delegate = self

      contactInfo = official.contactInfo

      setUpLabels()
      setUpPhoto()
   }

   // MARK: Functions
   func setUpLabels() {

      repName.text = official.name
      officeName.text = official.office.name
      party.text = official.party ?? ""

      switch party.text!.lowercased() {
         case "republican":
            party.textColor = UIColor.red
         case "democrat":
            party.textColor = UIColor.blue
      default:
         party.textColor = UIColor.black
      }
   }

   func setUpPhoto() {

      let noPhotoImage = UIImage(named: "George Washington")!.tranlucentWithAlpha(0.5)

      if let urlString = official.photoUrl {
         if let photoURL = URL(string: urlString) {
            photo.kf.indicatorType = .activity
            photo.kf.setImage(with: photoURL)
         }
      } else {
         photo.image = noPhotoImage
      }

      photo.contentMode = .scaleAspectFill
      photo.layer.cornerRadius = 15.0
      photo.layer.masksToBounds = true
   }

   func configureCell(_ cell: UITableViewCell, index: Int) {

      let contact = contactInfo![index]

      cell.imageView?.image = contact.icon
      cell.textLabel?.text = contact.id
      cell.accessoryType = .disclosureIndicator
      cell.imageView?.layer.cornerRadius = 5.0
      cell.imageView?.layer.masksToBounds = true
   }

   // MARK:  Contact Table Delegate/DataSource

   // Number Of Rows
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return official.contactInfo?.count ?? 0
   }

   // Cell For Row
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

      let cell = tableView.dequeueReusableCell(withIdentifier: "Contact Cell", for: indexPath)
      configureCell(cell, index: indexPath.row)
      return cell
   }

   // MARK: Table Delegate

   // Did Select
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

      tableView.deselectRow(at: indexPath, animated: false)

      let contact = contactInfo![indexPath.row]

      if let url = contact.url {
         UIApplication.shared.open(url)
      }

      return
   }
}

import Foundation
import CoreData
import SwiftyJSON

// Encapsulates all information about a government official

class Official: Equatable {

   var name = String()
   var office: Office!
   var party: String?
   var photoUrl: String?
   var photoFileName: String?

   var contactInfo: [Contact]? // e.g., phone, facebook

   fileprivate let json: JSON

   init(inJSON: JSON) {

      json = inJSON

      name = json["name"].string ?? "Name Unknown"

      // should be "Unknown" if there's no party given (?)
      if let party = json["party"].string {
         switch party {
         case "Democratic":
            self.party = "Democrat"
         case "Unknown":
            self.party = nil
         default:
            self.party = party
         }
      } else {
         self.party = nil
      }

      photoUrl = json["photoUrl"].string

      fillContactInfo(json)
   }

   fileprivate func fillContactInfo(_ inJSON: JSON) {

      // keep contactInfo nil if no contact info
      var hasContactInfo = false
      var tempContacts = [Contact]()

      if let phone = inJSON["phones"][0].string {
         let contact = Contact(type: ContactType.Phone, icon: UIImage(named: "Phone")!, id: phone)

         tempContacts.append(contact)
         hasContactInfo = true
      }
      if let email = inJSON["emails"][0].string {
         let contact = Contact(type: ContactType.Email, icon: UIImage(named: "Email")!, id: email)
         tempContacts.append(contact)
         hasContactInfo = true
      }
      if let url = inJSON["urls"][0].string {
         let contact = Contact(type: ContactType.Web, icon: UIImage(named: "Web")!, id: url)
         tempContacts.append(contact)
         hasContactInfo = true
      }

      if hasContactInfo {
         contactInfo = tempContacts
      }

      parseSocialMedia()
   }

   fileprivate func parseSocialMedia() {

      guard let socialChannels = json["channels"].array else {
         return
      }

      if contactInfo == nil {
         contactInfo = [Contact]()
      }

      for channel in socialChannels {

         if let type = channel["type"].string, let id = channel["id"].string {

            if let channelType = ContactType.typeForString(type) {

               let image = ContactType.iconForType(channelType)
               contactInfo!.append(Contact(type: channelType, icon: image, id: id))
            }
         }
      }
   }
}

func ==(lhs: Official, rhs: Official) -> Bool {
   return lhs.name == rhs.name
}

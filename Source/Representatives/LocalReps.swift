//
//  LocalReps.swift
//  Representative
//
//  Created by Dolemite on 4/17/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import Foundation
import CoreData

// Encapsulates sheriff, etc.
// Owned by Constituent, used by Local Table to organize sheriff, etc.
struct LocalReps {

   var officialArray = [Official]()

   init(officials: [Official]) {

      // filter out local and national (for some reason US Senate and House are listed as State)
      officialArray = officials.filter() { (official: Official) in

         return official.office.division.level == DivisionLevel.local
         }.sorted() { off1, off2 in
            return off1.office.name < off2.office.name
      }
   }
}

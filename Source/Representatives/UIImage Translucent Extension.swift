//
//  UIImage Translucent Extension.swift
//  Representative
//
//  Created by Dolemite on 7/5/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import Foundation
import UIKit

// So that placeholder images for representatives are translucent

extension UIImage {

   func tranlucentWithAlpha(_ alpha: CGFloat) -> UIImage {
      UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
      draw(at: CGPoint.zero, blendMode: .normal, alpha: alpha)
      let image = UIGraphicsGetImageFromCurrentImageContext()
      UIGraphicsEndImageContext()
      return image!
   }
}

//
//  StateReps.swift
//  Representative
//
//  Created by Dolemite on 4/17/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import Foundation
import CoreData

// Encapsulates state senators, etc.
// Owned by Constituent, used by State Table to organize Governor, etc.

struct StateReps {

    var headOfGov: Official!
    var attGeneral: Official!

    // Not every citizen has these:
    var deputyHeadOfGov: Official?
    var stateSenator: Official?
    var stateCongresspeople = [Official]()


    // the array for the table to use with common offices at the top
    var commonOfficials: [Official] {
        let officials = [headOfGov, deputyHeadOfGov, attGeneral, stateSenator].compactMap() { $0 }
        let congress = stateCongresspeople.compactMap() { $0 }
        return officials + congress
    }

    var others = [Official]()

    init(officials: [Official]) {

        // not every citizen has these:
        deputyHeadOfGov = nil
        stateCongresspeople = [Official]()
        stateSenator = nil

        // filter out local and national (for some reason US Senate and House are listed as State)
        let officials = officials.filter() { official in
            if let levels = official.office.levels, levels.contains("country") {
                return false
            } else {
                return official.office.division.level == DivisionLevel.state
            }
        }

        // Assign officials to common offices - this allows for rough sorting of officials
        for official in officials {

            assert(official.office.division.level == DivisionLevel.state)

            if let roles = official.office.roles {

                assert(roles.count == 1)

                let role = roles.first!

                switch role {
                case "headOfGovernment":
                    headOfGov = official
                case "deputyHeadOfGovernment":
                    deputyHeadOfGov = official
                case "legislatorUpperBody":
                    stateSenator = official
                case "legislatorLowerBody":
                    stateCongresspeople.append(official)
                default:
                    others.append(official)
                }

            } else if official.office.name.contains("Attorney General") {
                attGeneral = official
            } else {
                others.append(official)
            }
        }
    }
}

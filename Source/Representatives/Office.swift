
import Foundation
import CoreData
import SwiftyJSON

let ShortNames = [
   "President of the United States": "President",
   "Vice-President of the United States": "Vice President",
   "United States Senate": "Senator",
   "United States House of Representatives": "Congressperson"
]

// e.g., office of the Governor or President.  A single office, e.g. US Senator, can have multiple Officials.

open class Office {

   var name: String
   var levels: [String]? // e.g., Country
   var roles: [String]?  // e.g., Head of State
   var division: Division!

   var shortName: String {
      return ShortNames[name] ?? name
   }

   public init(json: JSON) {

      name = json["name"].stringValue

      if let levels = json["levels"].array {
         self.levels = levels.map() {
            $0.stringValue
         }
      }
      if let roles = json["roles"].array {
         self.roles = roles.map() {
            $0.stringValue
         }
      }

      // DC madness

      let state = (UIApplication.shared.delegate as! AppDelegate).constituent!.state

      guard state == "dc" else {
         return
      }

      if let level = levels?[0] {
         if let role = roles?[0] {
            if level == "country" && role == "legislatorLowerBody" {
               self.name = "Congressperson"
            }
            if level == "locality" && role == "legislatorUpperBody" {
               self.name = "Councilmember"
            }
         }
         if level == "locality" && self.name == "headofGovernment" {
            self.name = "Mayor"
         }
      }
   }
}

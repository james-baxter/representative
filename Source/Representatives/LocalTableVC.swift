//
// LocalTableVC.swift
//  Representative
//
//  Created by Dolemite on 4/10/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import UIKit
import CoreData

class LocalTableVC: UITableViewController, RepTable {

   var constituent: Constituent!

   var localReps = [Official]()

   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      localReps = constituent?.localReps.officialArray ?? []
   }

   // MARK: - Table view data source

   override func numberOfSections(in tableView: UITableView) -> Int {
      return 1
   }

   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return localReps.count
   }

   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

      let cell = tableView.dequeueReusableCell(withIdentifier: "Representative Cell", for: indexPath) as! RepresentativeCell

      cell.assignOfficial(localReps[indexPath.row])

      return cell
   }

   // MARK: - Table view delegate

   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      tableView.deselectRow(at: indexPath, animated: false)
      let detailView = storyboard?.instantiateViewController(withIdentifier: "Rep Info") as! RepInfoVC
      detailView.official = localReps[indexPath.row]
      navigationController?.pushViewController(detailView, animated: true)
   }


}

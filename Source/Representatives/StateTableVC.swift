//
//  StateTableVC.swift
//  Representative
//
//  Created by Dolemite on 4/10/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import UIKit
import CoreData

class StateTableVC: UITableViewController, RepTable {

   var constituent: Constituent!

   var stateReps = [[Official]]() // one array of common officials and one of miscellaneous

   override func viewWillAppear(_ animated: Bool) {

      let commonOfficials = constituent?.stateReps.commonOfficials ?? []
      var otherOfficials = constituent?.stateReps.others ?? []

      otherOfficials.sort() { off1, off2 in
         return off1.office.name < off2.office.name
      }

      stateReps = [commonOfficials, otherOfficials]

      if constituent.state == "dc" {
         navigationItem.title = "District"
      } else {
         navigationItem.title = "State"
      }
   }

   // MARK: - Table view data source

   override func numberOfSections(in tableView: UITableView) -> Int {
      return 2
   }

   override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
      switch section {
      case 0:
         return "Common Officials"
      case 1:
         return "Other Officials"
      default:
         return "" // Whoops
      }
   }

   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

      return stateReps[section].count
   }

   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

      let cell = tableView.dequeueReusableCell(withIdentifier: "Representative Cell", for: indexPath) as! RepresentativeCell

      cell.assignOfficial(stateReps[indexPath.section][indexPath.row])

      return cell
   }

   // MARK: - Table view delegate

   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      tableView.deselectRow(at: indexPath, animated: false)

      let detailView = storyboard?.instantiateViewController(withIdentifier: "Rep Info") as! RepInfoVC
      detailView.official = stateReps[indexPath.section][indexPath.row]

      navigationController?.pushViewController(detailView, animated: true)
   }

}

//
//  RepTable Protocol.swift
//  Representative
//
//  Created by Dolemite on 5/17/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import Foundation

// Allows generic segue to representative info page

protocol RepTable {
   var constituent: Constituent! { get set }
}

//
//  ContactType Enum.swift
//  Representative
//
//  Created by Dolemite on 4/28/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import Foundation
import UIKit

enum ContactType: String, CaseIterable {

    case Phone, Email, Web, Facebook, Twitter, YouTube

    static let urlSchemes: [ContactType: String] = [
        .Phone: "tel:", .Email: "mailto:", .Web: "", .Facebook: "https://www.facebook.com/",
        .Twitter: "https://twitter.com/", .YouTube: "https://www.youtube.com/"]

    var urlScheme: String {
        return ContactType.urlSchemes[self]!
    }

    static func iconForType(_ type: ContactType) -> UIImage {
        return UIImage(named: type.rawValue)!
    }

    static func typeForString(_ string: String) -> ContactType? {
        return ContactType(rawValue: string)
    }
}

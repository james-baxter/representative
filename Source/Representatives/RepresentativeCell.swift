//
//  RepresentativeCell.swift
//  Representative
//
//  Created by Dolemite on 4/13/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import UIKit
import Kingfisher


/// Cells in main tables that show basic representative info and photo
class RepresentativeCell: UITableViewCell {

   @IBOutlet weak var repPhoto: UIImageView!
   @IBOutlet weak var repName: UILabel!
   @IBOutlet weak var office: UILabel!

   var official: Official!

   /// Called by cellForRowAtIndexPath
   func assignOfficial(_ official: Official) {
      accessoryType = .disclosureIndicator
      self.official = official

      refreshFields()
   }

   func refreshFields() {
      repName.text = official.name
      office.text = official.office.name
      setUpImage()
   }

   func setUpImage() {
      repPhoto.image = nil
      if let _ = official.photoUrl, let url = URL(string: official.photoUrl!) {
         repPhoto.kf.indicatorType = .activity
         repPhoto.kf.cancelDownloadTask()
         repPhoto.kf.setImage(with: url)
      }

      repPhoto.contentMode = .scaleAspectFill
      repPhoto.layer.cornerRadius = 5.0;
      repPhoto.layer.masksToBounds = true;
   }
}

//
//  CountryTableVC.swift
//  Representative
//
//  Created by Dolemite on 4/10/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON

// Static table for president, etc.  DC is special case with no senators
class CountryTableVC: UITableViewController, RepTable {

   var constituent: Constituent!

   var state: String { return constituent!.state }
   var reps: NationalReps { return constituent!.nationalReps }

   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

      let section = indexPath.section
      let row = indexPath.row
      let cell = tableView.dequeueReusableCell(withIdentifier: "Representative Cell") as! RepresentativeCell

      switch section {
      case 0:
         if row == 0 {
            cell.assignOfficial(reps.president)
         } else {
            cell.assignOfficial(reps.vp)
         }
      case 1:
         if state == "dc" {
            cell.assignOfficial(reps.congressperson)
         } else {
            cell.assignOfficial(reps.senators![row])
         }
      case 2:
         if let _ = reps.congressperson {
            cell.assignOfficial(reps.congressperson)
         }
      default:
         NSLog("Too many sections in this table")
      }
      return cell
   }

   override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
      switch section {
      case 0:
         return "President & Vice President"
      case 1:
         return state == "dc" ? "Congress" : "Senate"
      case 2:
         return "Congress"
      default:
         NSLog("Too many sections")
         return ""
      }
   }

   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

      switch section {
      case 0:
         return 2  // pres & vp
      case 1:
         return state == "dc" ? 1 : 2 // congressperson if DC, else senators
      case 2:
            return 1  // congressperson if not DC
      default:
         NSLog("section greater than 2")
         return 3
      }
   }

   override func numberOfSections(in tableView: UITableView) -> Int {

         if reps.congressperson != nil && state != "dc" {
            return 3  // congressperson if not DC
         } else {
            return 2
         }
   }

   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      tableView.deselectRow(at: indexPath, animated: false)
      let detailView = storyboard?.instantiateViewController(withIdentifier: "Rep Info") as! RepInfoVC
      let cell = tableView.cellForRow(at: indexPath) as! RepresentativeCell
      detailView.official = cell.official
      navigationController?.pushViewController(detailView, animated: true)
   }
}

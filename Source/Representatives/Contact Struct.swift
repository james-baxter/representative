//
//  Contact Struct.swift
//  Representative
//
//  Created by Dolemite on 5/4/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import Foundation
import UIKit

// For "channels" key in JSON

struct Contact {

   let type: ContactType  // .Phone, .Twitter, etc.
   let icon: UIImage
   let id: String    // phone number, user name, etc.

   var url: URL? {
      let scheme = self.type.urlScheme
      let urlString = scheme + self.strippedID
      return URL(string: urlString)
   }

   // Removes non-digits from phone numbers because phone-type URLs require it
   var strippedID: String {

      guard type == .Phone else { return id }

      let utf16Array = id.utf16.filter() {
         CharacterSet.decimalDigits.contains(UnicodeScalar($0)!)
      }

      let characters = utf16Array.map() { Character(UnicodeScalar($0)!) }

      return characters.map( { String($0) } ).joined(separator: "")
   }
}

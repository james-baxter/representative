//
//  NationwideElectionsTVC.swift
//  Representative
//
//  Created by James Baxter on 2/11/20.
//  Copyright © 2020 James Baxter. All rights reserved.
//

import UIKit
import CoreData

class NationwideElectionsTVC: UITableViewController {

    var elections: [Election]!

    override func viewDidLoad() {
        super.viewDidLoad()

        let context = CoreDataStackManager.shared.mainContext
        elections  = NationwideElections.loadElections(using: context).sorted { $0.date < $1.date }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return elections.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NationwideElectionCell", for: indexPath)

        let election = elections[indexPath.row]

        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .none
        formatter.locale = Locale(identifier: "en_US")

        // Color date red if election is today
        let calendar = Calendar(identifier: .gregorian)
        let ymd = calendar.dateComponents([.year, .month, .day], from: election.date)
        let isToday = calendar.date(Date(), matchesComponents: ymd)
        cell.textLabel?.textColor = isToday ? .red : nil

        cell.textLabel?.font = UIFont.systemFont(ofSize: 15.0)
        cell.textLabel?.text = formatter.string(from: election.date)
        cell.detailTextLabel?.text = election.name

        return cell
    }
}

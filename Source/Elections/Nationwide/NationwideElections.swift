//
//  NationwideElections.swift
//  Representative
//
//  Created by James Baxter on 1/21/20.
//  Copyright © 2020 James Baxter. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreData


class NationwideElections: NSManagedObject {

    @NSManaged var rawJSON: Data?
    @NSManaged var date: Date

    var elections = [Election]()

    /// Get latest list of upcoming nationwide elections from Google API
    class func fetch(completionHandler: @escaping (Data?, String?) -> Void) {

        CIA_Client.shared.allElections { jsonData, errorString in
            guard errorString == nil else {  // errorString returned by URLSession
                completionHandler(nil, errorString)
                return
            }

            do {
                let json = try JSON(data: jsonData!)

                // URLSession succeeded but got an error message from server
                if let errorMessage = json["error"]["message"].string {
                    completionHandler(nil, errorMessage)
                } else {
                    completionHandler(jsonData, nil)
                }
            } catch {
                let errorMessage = "Error thrown while decoding nationwide election JSON data: \(error)"
                completionHandler(nil, errorMessage)
            }
        }
    }

    /// Convert JSON to array of Election structs
    func parse() {
        if let rawJSON = rawJSON {
            let json = try! JSON(data: rawJSON)
            let jsonElections = json["elections"]

            for (_, election): (String, JSON) in jsonElections {
                self.elections.append(Election(dict: election.dictionaryObject! as! [String: String]))
            }
            elections.sort()
        }
    }

    /// Get latest nationwide elections and save if changed.
    class func checkForUpdates(completionHandler: @escaping (_ fetchResult: UIBackgroundFetchResult) -> Void) {

        NationwideElections.fetch { data, errorString in
            guard errorString == nil else {
                completionHandler(UIBackgroundFetchResult.failed)
                return
            }

            assert(data != nil)

            let privateMOC = CoreDataStackManager.shared.createPrivateMOC()

            privateMOC.performAndWait {
                // previous elections
                let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NationwideElections.fetchRequest()
                let previousElectionsArray = try! privateMOC.fetch(fetchRequest) as! [NationwideElections]
                assert(previousElectionsArray.count <= 1)

                // new elections
                let newElections = NSEntityDescription.insertNewObject(forEntityName: "NationwideElections",
                                                                       into: privateMOC) as! NationwideElections
                newElections.rawJSON = data!
                newElections.date = Date()
                newElections.parse()

                var fetchResult: UIBackgroundFetchResult!

                // check if elections changed
                if let previousElections = previousElectionsArray.first {
                    previousElections.parse()
                    if previousElections.elections != newElections.elections {  // Elections changed
                        privateMOC.delete(previousElections)
                        fetchResult = .newData
                    } else {
                        privateMOC.delete(newElections)
                        fetchResult = .noData
                    }
                } else { // there are no previous elections stored and newElections will be saved
                    fetchResult = .newData
                }

                CoreDataStackManager.shared.saveContext(privateMOC)
                completionHandler(fetchResult)
            }
        }
    }

    class func loadElections(using context: NSManagedObjectContext,
                             includeTestElection: Bool = false) -> [Election] {

        var elections = [Election]()

        context.performAndWait {
            let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NationwideElections.fetchRequest()

            var nationwide = [NationwideElections]()

            do {
                nationwide = try context.fetch(fetchRequest) as! [NationwideElections]
            } catch let error {
                let failElectionParams = ["id": "0",
                                          "name": error.localizedDescription,
                                          "electionDay": "2020-01-01",
                                          "ocdDivisionId": ""]
                let failElection = Election(dict: failElectionParams)
                elections.append(failElection)
                return
            }

            assert(nationwide.count == 1)
            let nationwideElections = nationwide.first!
            nationwideElections.parse()

            if includeTestElection {
                elections = nationwideElections.elections
            } else {
                elections = nationwideElections.elections.filter { $0.id != 2000 }
            }
        }

        return elections
    }
}

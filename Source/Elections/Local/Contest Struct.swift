//
//  Contest Struct.swift
//  Representative
//
//  Created by Dolemite on 5/29/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import Foundation
import SwiftyJSON

// Each election has multiple contests, e.g., President, VP, Sheriff.
// Each contest has a group of candidates in the race.

struct Contest {
    
    let type: String?
    var candidates: [Candidate]? = nil
    let office: String?
    var primaryParty: String? = nil
    
    init?(json: JSON) {
        
        type = json["type"].string?.capitalized
        if type == "Primary", let party = json["primaryParty"].string {
            primaryParty = party.capitalized
        }
        
        if type == "Referendum" {
            office = json["referendumTitle"].string
        } else {
            office = json["office"].string
        }
        
        if let myCandidates = json["candidates"].array {
            candidates = [Candidate]()
            for candidate in myCandidates {
                let name = candidate["name"].string ?? "Name not found"
                let party = candidate["party"].string ?? "Unknown"
                candidates!.append(Candidate(name: name, party: party))
            }
        }
        
        // I require at least one of these or else it's "No Contest"
        if type == nil && office == nil && candidates == nil {
            return nil
        }
    }
}

//
//  ContestCell.swift
//  Representative
//
//  Created by Dolemite on 6/9/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import UIKit

// For table in election VC that lists various races voted on for a given election day

class ContestCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

   override func layoutSubviews() {
      super.layoutSubviews()
      textLabel!.frame.origin.x = 0
      detailTextLabel!.frame.origin.x = 0
   }

}

//
//  PollingLocation Struct.swift
//  Representative
//
//  Created by Dolemite on 5/29/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import Foundation
import SwiftyJSON

struct PollingLocation {

    let locationName: String?
    let pollingHours: String?
    let notes: String?
    let address: AddressStruct

    init?(json: JSON) {

        let addr = json["address"]

        guard addr.dictionary != nil else {
            return nil
        }

        notes = json["notes"].string
        pollingHours = json["pollingHours"].string
        locationName = addr["locationName"].string

        let street = addr["line1"].string ?? ""
        let city = addr["city"].string ?? ""
        let state = addr["state"].string ?? ""
        let zip = addr["zip"].string ?? ""

        address = AddressStruct(street: street, city: city, state: state, zip: zip)
    }

    static var errorLocation: PollingLocation {
        var locationInfo: [String: Any] = ["notes": "...",
                                           "pollingHours": "..."]
        let address =  [ "locationName": "Error finding polling location",
                         "line1": "",
                        "city": "",
                        "state": "",
                        "zip": ""]
        locationInfo["address"] = address
        let json = JSON(locationInfo)
        return PollingLocation(json: json)!
    }
}

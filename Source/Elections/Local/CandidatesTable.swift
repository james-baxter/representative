//
//  CandidatesTable.swift
//  Representative
//
//  Created by Dolemite on 6/9/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import UIKit

// Candidates in each electoral contest listed in election view controller

class CandidatesTable: UITableViewController {

   var candidates: [Candidate]!
   var contestName: String!

    override func viewDidLoad() {
      super.viewDidLoad()

    }

   override func numberOfSections(in tableView: UITableView) -> Int {
      return 1
   }

   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return candidates.count
   }

   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "Candidate Cell", for: indexPath)
      let candidate = candidates[indexPath.row]
      cell.textLabel!.text = candidate.name
      cell.detailTextLabel!.text = candidate.party
      return cell
   }

   override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
      return contestName
   }
}

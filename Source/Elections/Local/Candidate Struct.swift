//
//  Candidate Struct.swift
//  Representative
//
//  Created by Dolemite on 5/29/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Candidate {
   let name: String
   let party: String
}

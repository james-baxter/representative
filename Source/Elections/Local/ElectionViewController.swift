//
//  ElectionViewController.swift
//  Representative
//
//  Created by Dolemite on 6/8/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import UIKit
import MapKit
import EventKit
import EventKitUI


class ElectionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate {

    // MARK: - Outlets

    @IBOutlet weak var electionName: UILabel!
    @IBOutlet weak var electionDate: UILabel!
    @IBOutlet weak var locationName: UILabel!
    @IBOutlet weak var street: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var notes: UILabel!
    @IBOutlet weak var hours: UILabel!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var locationInfoStack: UIStackView!
    @IBOutlet weak var notesLabelsStack: UIStackView!
    @IBOutlet weak var notesLabel: UILabel!
    @IBOutlet weak var map: MKMapView?
    @IBOutlet weak var contestContainerView: UIView!
    @IBOutlet weak var reminderButton: UIButton!

    // MARK: - Variables

    var election: Election!
    var contestTable: UITableView!
    var placemark: CLPlacemark?
    var calendarManager: CalendarReminderManager?

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        contestTable = (children[0] as! UITableViewController).tableView
        contestTable.dataSource = self
        contestTable.delegate = self
        contestTable.backgroundColor = UIColor.clear

        setupPollingAddressFields()
        map?.delegate = self

        // Want to format reminder button, but not if need to ask for access
        let authStatus = EKEventStore.authorizationStatus(for: .event)
        if authStatus == .authorized {
            calendarManager = CalendarReminderManager()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        electionName.text = election.name
        electionDate.text = election.dateString

        if let _ = election.location {
            setUpMap()
        } else {
            removeMap()
        }

        setUpCalendarEventButton()
    }

    // MARK: - Calendar event

    func setUpCalendarEventButton() {
        guard let manager = calendarManager else {
            return
        }
        if let _ = manager.fetchExistingEvent(for: election) {
            reminderButton.setTitle("Edit calendar event", for: .normal)
        } else {
            reminderButton.setTitle("Create calendar event", for: .normal)
        }
    }

    @IBAction func pressedCalendarEventButton(_ sender: UIButton) {
        let calendarManager = CalendarReminderManager()
        let eventEditVC = calendarManager.createEventEditViewController(for: election)
        eventEditVC.editViewDelegate = self
        present(eventEditVC, animated: true, completion: nil)
    }

    // MARK: Location

    func setupPollingAddressFields() {

        let location = election.location
        locationName.text = location?.locationName ?? ""

        notes.text = location?.notes ?? ""
        hours.text = location?.pollingHours ?? ""
        let address = location?.address

        street.text = address?.street ?? ""

        // cityText includes state and zip
        var cityText = address?.city ?? ""
        cityText = cityText == "" ? "" : cityText + ", "
        var stateText = address?.state ?? ""
        stateText = stateText == "" ? "" : stateText + "  "
        let zipText = address?.zip ?? ""
        city.text = cityText + stateText + zipText

        if notes.text == "" {
            notesLabelsStack.removeArrangedSubview(notesLabel)
            notesLabel.removeFromSuperview()
        }
    }

    func setUpMap() {

        let address = election.location!.address
        let coder = CLGeocoder()
        let addressString = address.addressString(withPercentCoding: false)

        coder.geocodeAddressString(addressString) { placemarks, error in

            guard error == nil else {
                self.removeMap()
                return
            }

            let annotation = MKPointAnnotation()
            if let coordinate = placemarks?[0].location?.coordinate {
                self.placemark = placemarks![0]
                annotation.coordinate = coordinate
                self.map!.addAnnotation(annotation)
                var region = MKCoordinateRegion()
                region.center = annotation.coordinate
                region.span.latitudeDelta = 0.15
                region.span.longitudeDelta = 0.15
                region = self.map!.regionThatFits(region)
                self.map!.setRegion(region, animated: true)
                let tapRec = UITapGestureRecognizer(target: self, action: #selector(ElectionViewController.tappedMap))
                self.map!.addGestureRecognizer(tapRec)
            } else {
                self.removeMap()
            }
        }
    }

    @objc func tappedMap() {
        if let location = placemark {
            let mkPlacemark = MKPlacemark(placemark: location)
            let mapItem = MKMapItem(placemark: mkPlacemark)
            var span = MKCoordinateSpan()
            span.latitudeDelta = 0.15
            span.longitudeDelta = 0.15
            mapItem.openInMaps(launchOptions: [MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: span)])
        }
    }

    func removeMap() {
        if let map = map {
            DispatchQueue.main.async { map.removeFromSuperview() }
        }
    }

    // MARK: Contest table Data Source

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Contest Cell", for: indexPath) as! ContestCell

        cell.isUserInteractionEnabled = false
        cell.accessoryType = .none

        if let contests = election.contests {
            let contest = contests[indexPath.row]
            if let _ = contest.candidates {
                cell.isUserInteractionEnabled = true
                cell.accessoryType = .disclosureIndicator
            }
            let office = contest.office ?? "Contest office unknown"
            cell.textLabel!.text = office.capitalized

            var detailText = contest.type ?? ""
            if contest.type == "Primary", let party = contest.primaryParty {
                detailText += " (\(party))"
            }
            cell.detailTextLabel!.text = detailText

        } else {
            cell.textLabel!.text = "No Contest Information Available"
            cell.detailTextLabel!.text = ""
        }
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return election.contests?.count ?? 1
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    // MARK: Contest table Delegate

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let candidatesVC = storyboard?.instantiateViewController(withIdentifier: "Candidates Table") as! CandidatesTable
        let contest = election.contests![indexPath.row]
        candidatesVC.candidates = contest.candidates!
        candidatesVC.contestName = contest.office ?? "Contest Name Unknown"
        navigationController?.pushViewController(candidatesVC, animated: true)
    }
}

extension ElectionViewController: EKEventEditViewDelegate {

    #if DEBUG
    func eventEditViewControllerDefaultCalendar(forNewEvents controller: EKEventEditViewController) -> EKCalendar {
        let calendars = controller.eventStore.calendars(for: .event)
        var repCal: EKCalendar? = nil
        calendars.forEach { if $0.title == "Represent" { repCal = $0 } }
        return repCal ?? controller.eventStore.defaultCalendarForNewEvents!
    }
    #endif

    func eventEditViewController(_ controller: EKEventEditViewController, didCompleteWith action: EKEventEditViewAction) {
        controller.dismiss(animated: true, completion: nil)
    }
}

//
//  Representative
//
//  Created by James Baxter on 5/7/20.
//  Copyright © 2020 James Baxter. All rights reserved.
//

import Foundation
import UIKit
import EventKit
import EventKitUI


struct CalendarReminderManager {

    let eventStore = EKEventStore()

    /// Provides system UI screen for editing calendar event for election. 
    /// - Parameter election: Election
    /// - Returns: EKEventViewController
    func createEventEditViewController(for election: Election) -> EKEventEditViewController {
        let eventViewController = EKEventEditViewController()
        var event: EKEvent?
        if let fetchedEvent = fetchExistingEvent(for: election) {
            event = fetchedEvent
        } else {
            event = createCalendarEvent(for: election)
        }
        eventViewController.event = event
        eventViewController.eventStore = eventStore
        return eventViewController
    }

    /// Check if a reminder for this election already exists, and if so return it.
    func fetchExistingEvent(for election: Election) -> EKEvent? {
        let calendar = Calendar.current
        let dayBegin = calendar.date(bySettingHour: 0, minute: 0, second: 0,
                                     of: election.date, matchingPolicy: .previousTimePreservingSmallerComponents,
                                     repeatedTimePolicy: .first, direction: .backward)!
        let dayEnd = calendar.date(bySettingHour: 23, minute: 59, second: 59,
                                   of: election.date, matchingPolicy: .nextTime,
                                   repeatedTimePolicy: .first, direction: .forward)!
        let predicate = eventStore.predicateForEvents(withStart: dayBegin, end: dayEnd, calendars: nil)
        let events = eventStore.events(matching: predicate)
        var foundEvent: EKEvent?
        for event in events {
            if event.title == election.name {
                foundEvent = event
                break
            }
        }
        return foundEvent
    }

    /// Creates a default calendar event for an election, requesting authorization if needed
    /// - Parameter election: Election
    /// - Returns: the calendar event
    func createCalendarEvent(for election: Election) -> EKEvent? {
        let authStatus = EKEventStore.authorizationStatus(for: .event)
        guard authStatus == .authorized else {
            authStatus == .notDetermined ? requestAccess() : alertUserOfAuthStatus(status: authStatus)
            return nil
        }

        let event = EKEvent(eventStore: eventStore)
        event.title = election.name
        event.startDate = election.date
        event.endDate = election.date
        event.location = election.location?.address.addressString(withPercentCoding: false)
        let day: TimeInterval = 24 * 60 * 60
        let alarm1 = EKAlarm(relativeOffset: 0.0)
        let alarm2 = EKAlarm(relativeOffset: -1.0 * day)
        event.alarms = [alarm1, alarm2]

        return event
    }

    /// Calls method on Event Store that prompts user for authorization to access calendar
    func requestAccess() {
        eventStore.requestAccess(to: .event) { _, error in
            if let error = error { self.alertUserOf(error: error) }
        }
    }

    /// Called to alert user that calendar access is restricted or previously denied, prompting user to go to Settings
    /// - Parameter status: EKAuthorizationStatus, probably .denied, but possibly .restricted
    func alertUserOfAuthStatus(status: EKAuthorizationStatus) {
        DispatchQueue.main.async {
            let message = """
                This app is not authorized to access your calendar.  You may be able to change this in Settings.
            """
            let title = "Calendar Access Denied"
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let goToSettings: (UIAlertAction) -> Void = { _ in
                let settingsURL = URL(string: UIApplication.openSettingsURLString)!
                UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
            }
            alertController.addAction(UIAlertAction(title: "Settings", style: .default, handler: goToSettings))
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            alertController.present()
        }
    }

    /// For unusual errors, e.g. when requesting access to event store
    func alertUserOf(error: Error) {
        DispatchQueue.main.async {
            let message = "An error occurred trying to get calendar access: \n\"\(error.localizedDescription)\""
            let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            alertController.present()
        }
    }
}

//
//  Election Struct.swift
//  Representative
//
//  Created by Dolemite on 5/29/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import Foundation
import SwiftyJSON

/*
{  "id": "2000",
"name": "VIP Test Election",
"electionDay": "2021-06-06",
"ocdDivisionId": "ocd-division/country:us"
}
*/

struct Election {
    let id: Int
    let name: String
    let dateString: String
    let ocdDivisionID: String

    var date: Date {
        let dateArray = dateString.split(separator: "-").map { Int($0) }
        let components = DateComponents(calendar: Calendar(identifier: .gregorian),
            year: dateArray[0],
            month: dateArray[1],
            day: dateArray[2],
            hour: 9,
            minute: 0)
        return components.date!
    }

    var location: PollingLocation?
    var contests: [Contest]?

    init(dict: [String: String]) {
        id = Int(dict["id"]!)!
        name = dict["name"]!
        dateString = dict["electionDay"]!
        ocdDivisionID = dict["ocdDivisionId"]!
    }

    init(electionJSON: JSON) {
        self.init(dict: electionJSON.dictionaryObject! as! [String: String])
    }

    init(json: JSON) {

        let electionJSON = json["election"]
        self.init(electionJSON: electionJSON)

        let locations = json["pollingLocations"].array

        if let locs = locations, locs.count == 1 {
            location = PollingLocation(json: locs[0])
        } else {
            location = PollingLocation.errorLocation
        }

        if let myContests = json["contests"].array {
            for contest in myContests {
                if let c = Contest(json: contest) {
                    if contests == nil {
                        contests = [Contest]()
                    }
                    contests!.append(c)
                }
            }
        }
    }
}

extension Election: Equatable {
    static func == (lhs: Election, rhs: Election) -> Bool {
        return lhs.id == rhs.id &&
            lhs.name == rhs.name &&
            lhs.dateString == rhs.dateString &&
            lhs.ocdDivisionID == rhs.ocdDivisionID
    }
}

extension Election: Comparable {
    static func <(lhs: Election, rhs: Election) -> Bool {
        return lhs.id < rhs.id
    }
}

extension Election {
    static func IDFromJSON(json: JSON) -> Int {
        return Int(json["id"].stringValue)!
    }
}


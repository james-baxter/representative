//
//  MainPage.swift
//  Representative
//
//  Created by Dolemite on 5/2/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import UIKit
import CoreData

// Main menu: Select Election or national, state, or local representatives

class MainPageTableViewController: UITableViewController {

    @IBOutlet weak var electionCell: UITableViewCell!

    var constituent: Constituent? {
        return (UIApplication.shared.delegate as! AppDelegate).constituent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNationwideElectionsCell()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
        setUpStaticCells()
    }

    // MARK: - Cell Setup

    func setUpStaticCells() {

        var cells = [UITableViewCell]()
        for repRow in 0...2 {
            cells.append(tableView(tableView, cellForRowAt: IndexPath(row: repRow, section: 0)))
        }

        // constituent's upcoming elections
        cells.append(tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 1)))

        let messages = ["National Data Missing", "State Data Missing", "Local Data Missing", "No Upcoming Elections"]
        var labels = ["U.S.", "State", "Local"]

        if let state = constituent?.state, state == "dc" {
            labels[1] = "District"
        }

        if let _ = constituent?.nationalReps {
            enableCell(cells[0], label: labels[0], isElectionCell: false)
        } else {
            disableCell(cells[0], message: messages[0], isElectionCell: false)
        }

        if let _ = constituent?.stateReps {
            enableCell(cells[1], label: labels[1], isElectionCell: false)
        } else {
            disableCell(cells[1], message: messages[1], isElectionCell: false)
        }

        if let localReps = constituent?.localReps, localReps.officialArray.count > 0 {
            enableCell(cells[2], label: labels[2], isElectionCell: false)
        } else {
            disableCell(cells[2], message: labels[2], isElectionCell: false)
        }

    }

    func setUpNationwideElectionsCell() {

        NationwideElections.checkForUpdates() { variable in
            if variable == .failed {
                DispatchQueue.main.async {
                    self.presentAlert("Failed to refresh nationwide elections", handler: nil)
                }
            }
            let context = CoreDataStackManager.shared.mainContext
            let elections = NationwideElections.loadElections(using: context)

            DispatchQueue.main.async {
                let nationwideElectionCell = self.tableView(self.tableView, cellForRowAt: IndexPath(row: 0, section: 2))
                if !elections.isEmpty {
                    self.enableCell(nationwideElectionCell, label: "\(elections.count) upcoming elections",
                        isElectionCell: false)
                } else {
                    self.disableCell(nationwideElectionCell, message: "No upcoming elections", isElectionCell: false)
                }
            }
        }
    }

    func enableCell(_ cell: UITableViewCell, label: String, isElectionCell: Bool) {

        cell.accessoryType = .disclosureIndicator
        cell.isUserInteractionEnabled = true
        cell.textLabel!.adjustsFontSizeToFitWidth = true
        cell.textLabel!.isEnabled = true
        cell.textLabel!.text = label
    }

    func disableCell(_ cell: UITableViewCell, message: String, isElectionCell: Bool) {
        cell.accessoryType = .none
        cell.isUserInteractionEnabled = false
        cell.textLabel!.text = message
        cell.textLabel!.isEnabled = false
    }

    // MARK: - Table View Data Source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return constituent?.elections?.count ?? 1
        } else {
            return super.tableView(tableView, numberOfRowsInSection: section)
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard indexPath.section == 1 else {
            return super.tableView(tableView, cellForRowAt: indexPath)
        }

        let reuseID = "dynamic election cell"
        var cell: UITableViewCell
        if let reusedCell = tableView.dequeueReusableCell(withIdentifier: "dynamic election cell") {
            cell = reusedCell
        } else {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: reuseID)
            cell.accessoryType = .disclosureIndicator
        }

        if let constituent = constituent {
            if let elections = constituent.elections, !elections.isEmpty {
                cell.accessoryType = .disclosureIndicator
                cell.isUserInteractionEnabled = true
                cell.textLabel!.isEnabled = true
                cell.detailTextLabel!.isEnabled = true
                let election = elections[indexPath.row]
                cell.textLabel!.text = election.name
                cell.detailTextLabel!.text = election.dateString
                cell.textLabel?.font = UIFont.systemFont(ofSize: 14.0)
                cell.textLabel?.lineBreakMode = .byTruncatingMiddle
            } else {  // no upcoming elections for this voter's address
                cell.accessoryType = .none
                cell.isUserInteractionEnabled = false
                cell.textLabel!.text = "No upcoming elections"
                cell.textLabel!.isEnabled = false
                cell.detailTextLabel!.isEnabled = false
                cell.detailTextLabel!.text = ""
            }
        } else {  // no constituent, no voter address set
            cell.accessoryType = .none
            cell.isUserInteractionEnabled = false
            cell.textLabel!.adjustsFontSizeToFitWidth = true
            cell.textLabel!.text = "Enter a voter's address to check for elections"
            cell.textLabel!.isEnabled = false
            cell.detailTextLabel!.isEnabled = false
            cell.detailTextLabel!.text = ""
        }

        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }

    override func tableView(_ tableView: UITableView, indentationLevelForRowAt indexPath: IndexPath) -> Int {
        return 0
    }

    // MARK: - Table View Delegate, Etc.

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        guard let constituent = constituent else {
            return
        }

        switch (indexPath.section, indexPath.row) {
        case (0, 0):
            if let _ = constituent.nationalReps {
                performSegue(withIdentifier: "Country", sender: self)
            } else {
                self.presentAlert("There was an error retrieving this data", handler: nil)
                tableView.deselectRow(at: indexPath, animated: true)
            }
        case (0, 1):
            if let _ = constituent.stateReps {
                performSegue(withIdentifier: "State", sender: self)
            }
        case (0, 2):
            if let _ = constituent.localReps {
                performSegue(withIdentifier: "Local", sender: self)
            }
        case (1, let row):
            if let elections = constituent.elections, let electionVC = storyboard?.instantiateViewController(
                withIdentifier: "ElectionVC") as? ElectionViewController {
                electionVC.election = elections[row]
                navigationController?.pushViewController(electionVC, animated: true)
            }
        case (2, 0):
            if let nationwideElectionVC = storyboard?.instantiateViewController(
                withIdentifier: "NationwideElectionsTVC") as? NationwideElectionsTVC {
                navigationController?.pushViewController(nationwideElectionVC, animated: true)
            }
        default:
            break
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // RepTable protocol requires constituent property
        var destVC = segue.destination as? RepTable
        if destVC != nil {
            destVC!.constituent = constituent // nil will cause test election to be shown
        }
    }

    func presentAlert(_ string: String, handler: ((_ alert: UIAlertAction) -> Void)?) {
        let alert = UIAlertController(title: "Alert", message: string, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: handler))
        present(alert, animated: true, completion: nil)
    }

}

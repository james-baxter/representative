//
//  Main Page.swift
//  Representative
//
//  Created by Ekstasis on 7/27/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import UIKit

class Main_Page: UIViewController {

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var address2Label: UILabel!

    var constituent: Constituent? {
        (UIApplication.shared.delegate as! AppDelegate).constituent
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // No data on disk, first time running
        if constituent == nil {
            let alertMessage = "In order to get started, you will need to enter an address for a registered voter."
            presentAlert(alertMessage) { _ in
                // alert completion will just press the "new location" button for us
//                self.newAddress(UIButton())
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let constituent = constituent, let address = constituent.addressStruct {
            addressLabel.text = address.addressString(withPercentCoding: false)
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.constituent = nil
            addressLabel.text = "Click icon above to enter a voter's address"
        }

    }

    @IBAction func newAddress(_ sender: AnyObject) {

        let addressForm = self.storyboard?.instantiateViewController(withIdentifier: "Address Form") as! AddressFormVC
        navigationController?.pushViewController(addressForm, animated: true)
    }

    func presentAlert(_ string: String, handler: ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController.create(string, actionHandler: handler)
        alert.present()
    }
}

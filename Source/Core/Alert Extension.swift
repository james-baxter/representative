//
//  Alert Extension.swift
//  OnTheMap
//
//  Created by Dolemite on 1/2/16.
//  Copyright © 2016 Baxter Heavy Industries. All rights reserved.
//

import UIKit

// Reusable alert with custom handler that can be called in one VC but show up in another
// Inspired by stackoverflow post I lost track of

extension UIAlertController {

   class func create(_ message: String, actionHandler: ((UIAlertAction) -> Void)?) -> UIAlertController  {
      let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
      alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: actionHandler))
      return alertController
   }

   func present() {
      if let rootVC = UIApplication.shared.keyWindow?.rootViewController {
         presentFromController(rootVC, animated: true)
      }
   }

   fileprivate func presentFromController(_ controller: UIViewController, animated: Bool) {

      switch controller {

      case let navVC as UINavigationController:
         if let visibleVC = navVC.visibleViewController {
            presentFromController(visibleVC, animated: animated)
         }
      case let tabVC as UITabBarController:
         if let selectedVC = tabVC.selectedViewController {
            presentFromController(selectedVC, animated: animated)
         }
      default:
         DispatchQueue.main.async {
            controller.present(self, animated: animated, completion: nil)
         }
      }
   }
}

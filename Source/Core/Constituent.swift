//
//  Constituent.swift
//  Representative
//
//  Created by Dolemite on 4/10/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

/*
 * Constituent is the central class, saved to disk 
 * and owns and manages the voter's representatives, elections, and address.
 *
 * Election and representative JSON is saved to Core Data as NSData.
 */

class Constituent: NSManagedObject {

    // MARK: - Managed Variables

    @NSManaged var address: ManagedAddress!

    var addressStruct: AddressStruct? {
        var addy: AddressStruct?
        mainContext.performAndWait {
            addy = address?.addressStruct
        }
        return addy
    }

    // Raw JSON is saved to disk and unpacked on app launch
    @NSManaged var rawRepJSON: Data!
    @NSManaged var rawElectionJSON: Data?

    @NSManaged var congressDistict: NSNumber!
    @NSManaged var councilDist: NSNumber!
    @NSManaged var county: String!

    // MARK: - Variables
    var nationalReps: NationalReps!
    var stateReps: StateReps!
    var localReps: LocalReps!
    var elections: [Election]?
    var useTestElection = false

    var repJSON: JSON?

    let cd = CoreDataStackManager.shared
    let mainContext = CoreDataStackManager.shared.mainContext

    var state: String {
        var st = String()
        mainContext.performAndWait() {
            st = self.address?.state ?? ""
        }
        return st
    }

    // MARK: - Initializers
    override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    init(context: NSManagedObjectContext) {
        let entity = NSEntityDescription.entity(forEntityName: "Constituent", in: context)
        super.init(entity: entity!, insertInto: context)
    }

    // MARK: - Representatives

    // Called by Address Form for new address
    func getRepInfo(_ address: AddressStruct, completion: @escaping (_ json: JSON?, _ errorString: String?) -> Void) -> URLSessionDataTask {

        // Download API data
        let task = CIA_Client.shared.getRepresentativeInfo(address) { jsonData, errorString in

            var json: JSON? = nil
            var normalizedInput: JSON? // Google parses the address and returns it

            // If received response from Google with info or JSON describing error
            if let data = jsonData {
                do {
                    json = try JSON(data: data)
                    normalizedInput = json!["normalizedInput"]
                } catch {
                    completion(nil, "Exception while trying to decode Data to JSON")
                    return
                }
            }

            // E.g., bad network or 400 bad address type error
            guard errorString == nil else {
                completion(json, errorString)
                return
            }

            // API returned "valid" JSON, but with nothing but normalized Input (eg, street = "ga", no other address info)
            if json!["officials"].isEmpty {
                completion(normalizedInput, "There appear to be no representatives for this address")
                return
            }

            self.mainContext.performAndWait() {
                self.rawRepJSON = jsonData!
                self.rawElectionJSON = nil
                self.repJSON = json
                let addr = AddressStruct(json: normalizedInput!)
                self.address = ManagedAddress(address: addr, context: self.mainContext)
                self.address.constituent = self
            }

            self.parseRepresentativeInfo()

            completion(nil, nil) // AddressFormVC dismisses and saves context
        }
        return task
    }

    // Main parsing of JSON for representative info, called directly by appDelegate for saved constituent
    func parseRepresentativeInfo() {

        var divisions = [Division]()
        var offices = [Office]()
        var officials = [Official]()

        mainContext.performAndWait() {

            if self.repJSON == nil { self.repJSON = try! JSON(data: self.rawRepJSON) }

            let divisionsJSON = self.repJSON!["divisions"]
            let officesJSON = self.repJSON!["offices"]
            let officialsJSON = self.repJSON!["officials"]

            // OFFICIALS
            for officialJSON in officialsJSON.array! {
                let official = Official(inJSON: officialJSON)
                officials.append(official)
            }

            // OFFICES and tie officials to office and vice versa
            for officeJSON in officesJSON.array! {
                let office = Office(json: officeJSON)
                for officialIndex in officeJSON["officialIndices"].arrayObject as! [Int] {
                    let official = officials[officialIndex]
                    official.office = office
                }
                offices.append(office)
            }

            // DIVISIONS and tie office to division and vice versa
            for (ocdID, divisionJSON) in divisionsJSON {
                let division = Division(ocdId: ocdID, json: divisionJSON)
                // Some divisions have no offices, e.g., place=Pleasantville
                if let officeIndices = divisionJSON["officeIndices"].arrayObject as? [Int] {
                    division.offices = [Office]()
                    for index in officeIndices {
                        let office = offices[index]
                        office.division = division
                        division.offices?.append(office)
                    }
                }
                divisions.append(division)
            }

            self.nationalReps = NationalReps(officials: officials)
            self.stateReps = StateReps(officials: officials)
            self.localReps = LocalReps(officials: officials)
        }
    }

    // MARK: - Elections

    /// Downloads JSON relating to elections for a specific address, then calls "parseElectionInfo".
    /// Called when user hits "submit" for a new address.
    func getElectionInfo(_ addr: AddressStruct, errorHandler: @escaping (_ errorString: String?) -> Void) {

        CIA_Client.shared.getElectionInfo(addr) { jsonData, errorString in

            guard errorString == nil else {
                errorHandler(errorString)
                return
            }

            do {
                // TODO: TODO check this forced unwrap is a good idea
                let electionJSON = try JSON(data: jsonData!)

                guard electionJSON["error"].isEmpty else {
                    self.mainContext.perform() {
                        self.rawElectionJSON = nil
                        self.elections = nil
                    }

                    // No election comes back as 400/invalid/election unknown
                    let code = electionJSON["error"]["code"].int
                    if code == 400 {
                        errorHandler(nil)
                    } else {
                        errorHandler("Election JSON Error")
                    }
                    return
                }

                self.mainContext.performAndWait() {
                    // TODO: check this forced unwrap is a good idea
                    self.rawElectionJSON = jsonData!
                }
                CoreDataStackManager.shared.saveContext(self.mainContext)

                self.parseElectionInfo()

                errorHandler(nil) // AddressFormVC dismisses itself and saves context

            } catch {
                errorHandler("Exception thrown while decoding election JSON data")
                return
            }
        }
    }

    // Main parsing of JSON for election info, called directly by appDelegate for saved constituent
    func parseElectionInfo() {

        var rawData: Data?
        mainContext.performAndWait() {
            rawData = self.rawElectionJSON
        }
        guard rawData != nil else {
            elections = nil
            return
        }

        let json = try! JSON(data: rawData!)
        assert(json["error"].isEmpty)

        elections = [Election]()
        elections!.append(Election(json: json))

        let otherElections = json["otherElections"]
        otherElections.forEach { (_, electionJSON) in
            elections!.append(Election(electionJSON: electionJSON))
        }

        elections!.sort { $0.date < $1.date }
    }
}

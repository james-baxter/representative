//
//  AppDelegate.swift
//  Representative
//
//  Created by Dolemite on 4/8/16.
//  Copyright © 2016 James Baxter. All rights reserved.
//

import CoreData
import SwiftyJSON
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    // MARK: - Global Constants
    let networkTimeout = 10.0
    
    // MARK: - Properties
    var window: UIWindow?
    var constituent: Constituent?
    let coreDataManager = CoreDataStackManager.shared
    let mainContext = CoreDataStackManager.shared.mainContext
    
    // MARK: - UIApplicationDelegate
    
    func application(_: UIApplication, didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        UIApplication.shared.setMinimumBackgroundFetchInterval(3600 * 4)
        
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound]) { granted, error in }
        
        // Retrieve persistent Constituent if available
        if let fetchedConstituent = fetchConstituent() {
            constituent = fetchedConstituent
            constituent!.parseRepresentativeInfo()
            constituent!.parseElectionInfo()
        }
        
        return true
    }
    
    func applicationWillTerminate(_: UIApplication) {
        coreDataManager.saveContext(mainContext)
    }
    
    func application(_ application: UIApplication,
                     performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        NationwideElections.checkForUpdates { backgroundFetchResult in
            if backgroundFetchResult == .newData {
                self.notifyUser(message: "Nationwide elections updated")
            }
            completionHandler(backgroundFetchResult)
        }
    }
    
    // MARK: - Functions
    
    func notifyUser(message: String) {
        let content = UNMutableNotificationContent()
        content.title = message
        content.sound = UNNotificationSound.default
        
        let request = UNNotificationRequest(
            identifier: "com.representative.nationwide.election.update",
            content: content,
            trigger: nil)
        
        let center = UNUserNotificationCenter.current()
        center.add(request) { (error: Error?) in }
    }
    
    func fetchConstituent() -> Constituent? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Constituent")
        fetchRequest.sortDescriptors = []
        
        do {
            let fetchedConstituent = try mainContext.fetch(fetchRequest).first as? Constituent
            return fetchedConstituent
        } catch {
            return nil
        }
    }
    
    func testNotification() {
        let content = UNMutableNotificationContent()
        content.title = "Test"
        content.sound = UNNotificationSound.default
        
        let request = UNNotificationRequest(
            identifier: "com.representative.testNotification",
            content: content,
            trigger: UNTimeIntervalNotificationTrigger(timeInterval: 4.0, repeats: false))
        
        let center = UNUserNotificationCenter.current()
        center.add(request) { (error: Error?) in }
    }
}
